#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>


struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct pixel {
    uint8_t b, g, r;
};


struct image* create(uint64_t width, uint64_t height);

void deinitialize(struct image* img);